```agent any``` execute the pipeline or a specific stage on any available agent.

```agent none``` is used to disable global agent for the entire pipeline. Instead, we should define the agent in each stage.

```agent label``` is used to execute the whole pipeline or a specific stage on a node with a specific label

```agent docker``` is used to execute the entire pipeline or a specific stage inside a docker container

```agent dockerfile``` this directive is a special Docker related directive that is used to build a docker image from an existing Dockerfile within the source of the project and run the entire pipeline or a specific stage inside an instance of this built image. To use this special directive, you should connect Jenkins to your source code repository to able Jenkins to fetch the project source in conjunction with Dockerfile

